import bl00mbox
import random
import time
import math
import leds

from st3m import InputState, Responder
from st3m.application import Application, ApplicationContext
from st3m.ui.colours import BLUE, WHITE
from st3m.goose import Optional
from st3m.utils import xy_from_polar, tau
from st3m.ui.view import ViewManager
from st3m.ui.interactions import CapScrollController
from ctx import Context

import badgelink
import machine
from collections import deque


class MidiMessage:
    NOTE_OFF = 0x80
    NOTE_ON = 0x90


class MIDI(Responder):
    """Simple MIDI handler, currently only supports NOTE_ON and NOTE_OFF. Further extensions should
       be trivial, though"""
    def __init__(self) -> None:
        self.jack = badgelink.right
        self.jack.enable()
        self.midi = machine.UART(1, 31250, tx=self.jack.ring.pin.num(), rx=self.jack.tip.pin.num())
        self.buffer = b''
        self.queue = deque((), 128)

    def think(self, ins: InputState, delta_ms: int) -> None:
        new = self.midi.read()
        if new is not None:
            self.buffer = self.buffer + new
            while len(self.buffer) > 0:
                if self.buffer[0] & 0xF0 == MidiMessage.NOTE_OFF:
                    if len(self.buffer) < 3:
                        break
                    self.queue.append((MidiMessage.NOTE_OFF, int(self.buffer[0]) & 0x0F, self.buffer[1], self.buffer[2]))
                    print((MidiMessage.NOTE_ON, int(self.buffer[0]) & 0x0F, self.buffer[1], self.buffer[2]))
                    self.buffer = self.buffer[3:]
                elif self.buffer[0] & 0xF0 == MidiMessage.NOTE_ON:
                    if len(self.buffer) < 3:
                        break
                    self.queue.append((MidiMessage.NOTE_ON, int(self.buffer[0]) & 0x0F, self.buffer[1], self.buffer[2]))
                    print((MidiMessage.NOTE_ON, int(self.buffer[0]) & 0x0F, self.buffer[1], self.buffer[2]))
                    self.buffer = self.buffer[3:]
                else:
                    self.buffer = self.buffer[1:]

    def get(self):
        try:
            return self.queue.popleft()
        except IndexError:
            return None

    def amount(self) -> int:
        return len(self.queue)


class Monophonic(Responder):
    """Monophonic instrument controller using MIDI. Makes sure only the most recently pressed
       key is being played but also saves all pressed keys (and their order) so when releasing
       the currently played tone, the next most-recent one is being played.
       
       TODO: Add velocity support"""
    def __init__(self, midi: MIDI) -> None:
        self.midi = midi
        self.note_stack = list()

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.midi.think(ins, delta_ms)
        while self.midi.amount():
            m = self.midi.get()
            if m[0] == MidiMessage.NOTE_ON:
                if m[2] not in self.note_stack:
                    self.note_stack.append(m[2])
            if m[0] == MidiMessage.NOTE_OFF:
                try:
                    self.note_stack.remove(m[2])
                except ValueError:
                    pass

    def get_status(self) -> int or None:
        if len(self.note_stack) > 0:
            return self.note_stack[-1]
        else:
            return None


class Blob(Responder):
    def __init__(self) -> None:
        self._yell = 0.0
        self._blink = False
        self._blinking: Optional[int] = None

    def think(self, ins: InputState, delta_ms: int) -> None:
        if self._blinking is None:
            if random.random() > 0.99:
                self._blinking = 100
        else:
            self._blinking -= delta_ms
            if self._blinking < 0:
                self._blinking = None

    def draw(self, ctx: Context) -> None:
        blink = self._blinking is not None
        v = self._yell
        if v > 1.0:
            v = 1.0
        if v < 0:
            v = 0

        v /= 1.5
        if v < 0.1:
            v = 0.1

        ctx.rgb(62 / 255, 159 / 255, 229 / 255)

        ctx.save()
        ctx.rotate(-v)
        ctx.arc(0, 0, 80, tau, tau / 2, 1)
        ctx.fill()

        ctx.gray(60 / 255)
        if blink:
            ctx.line_width = 1
            ctx.move_to(50, -30)
            ctx.line_to(70, -30)
            ctx.stroke()
            ctx.move_to(30, -20)
            ctx.line_to(50, -20)
            ctx.stroke()
        else:
            ctx.arc(60, -30, 10, 0, tau, 0)
            ctx.arc(40, -20, 10, 0, tau, 0)
            ctx.fill()
        ctx.restore()

        ctx.arc(0, 0, 80, v / 2, tau / 2 + v / 2, 0)
        ctx.fill()

        ctx.rectangle(-80, 0, 20, -120)
        ctx.fill()


class Otamatone(Application):
    """
    A friendly lil' guy that is not annoying at all.
    """

    PETAL_NO = 7

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._ts = 0
        self._blob = Blob()

        self._blm = bl00mbox.Channel("Otamatone")

        # Sawtooth oscillator
        self._osc = self._blm.new(bl00mbox.patches.tinysynth)
        # Distortion plugin used as a LUT to convert sawtooth into custom square
        # wave.
        self._dist = self._blm.new(bl00mbox.plugins._distortion)
        # Lowpass.
        self._lp = self._blm.new(bl00mbox.plugins.lowpass)

        # Wire sawtooth -> distortion -> lowpass
        self._osc.signals.output = self._dist.signals.input
        self._dist.signals.output = self._lp.signals.input
        self._lp.signals.output = self._blm.mixer

        # Closest thing to a waveform number for 'saw'.
        self._osc.signals.waveform = 20000
        self._osc.signals.attack = 1
        self._osc.signals.decay = 0

        # Built custom square wave (low duty cycle)
        table_len = 129
        self._dist.table = [
            32767 if i > (0.1 * table_len) else -32768 for i in range(table_len)
        ]

        self._lp.signals.freq = 4000

        self._intensity = 0.0
        self.note = 0

        self.input.captouch.petals[self.PETAL_NO].whole.repeat_disable()
        self.midi = MIDI()
        self.mono = Monophonic(self.midi)

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        for i in range(26, 30 + 1):
            leds.set_rgb(i, 62, 159, 229)
        leds.update()

    def draw(self, ctx: Context) -> None:
        ctx.save()
        ctx.move_to(0, 0)
        ctx.gray(0)
        ctx.rectangle(-120, -120, 240, 240)
        ctx.fill()

        self._blob.draw(ctx)

        ctx.restore()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self._ts += delta_ms
        self._blob.think(ins, delta_ms)
        self.mono.think(ins, delta_ms)

        petal = self.input.captouch.petals[self.PETAL_NO]
        pos = ins.captouch.petals[self.PETAL_NO].position
        ctrl = pos[0] / 40000
        if ctrl < -1:
            ctrl = -1
        if ctrl > 1:
            ctrl = 1
        ctrl *= -1

        notedest = self.mono.get_status()

        if notedest is not None:
            # Tone in flow3r is semitones relative to A4 (440Hz)
            # In MIDI, A4 is note value 69
            # Hence, subtract 69 from MIDI value to get half-tone offset from A4
            notedest = notedest - 69
            self.note = self.note * 0.8 + notedest * 0.2
            tone = self.note if (abs(self.note - notedest) > 0.1) else notedest
            if self._intensity < 1.0:
                self._intensity += 0.1 * (delta_ms / 20)
            self._osc.signals.pitch.tone = tone
            self._osc.signals.trigger.start()
        else:
            self._intensity = 0
            self._osc.signals.trigger.stop()

        self._blob._yell = self._intensity * 0.8 + (ctrl + 1) * 0.1


if __name__ == "__main__":
    from st3m.run import run_view

    run_view(Otamatone(ApplicationContext()))
